from urllib.request import urlopen
from bs4 import BeautifulSoup
import time
from win10toast import ToastNotifier


def i9Search():
    url = "https://lista.mercadolivre.com.br/i9-9900ks"
    page = urlopen(url).read()
    soup = BeautifulSoup(page, 'html.parser')

    result = soup.find("ol", {"id": "searchResults"}).find_all("li")
    i9s = len(result) / 2
    print(i9s)
    if i9s != 1:
        toast = ToastNotifier()
        toast.show_toast("i9 9900ks", "ML com novo i9 9900ks", duration=60)
    time.sleep(300)


if __name__ == '__main__':
    while True:
        i9Search()
